Copy the token below and run the submission script included in the assignment download. When prompted, use your email address coursera@h9h.de.

pbIZviLrNHXlXSd2
Generate new token

Your submission token is unique to you and should not be shared with anyone. You may submit as many times as you like.